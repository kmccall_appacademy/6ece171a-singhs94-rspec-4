class Dictionary
  # TODO: your code goes here!
  attr_accessor :entries
  def initialize
    @entries = {}
  end
  def add(word)
    if word.is_a?(Hash)
      @entries.merge!(word)
    else
      @entries = {word => nil}
    end
  end

  def include?(word)
    @entries.each {|key, value| key == word ? (return true) : false}
		false
  end

  def find(word)
    @entries.select {|key, value| key.start_with? word}
  end
  def keywords
    keys = []
		i=0
		@entries = @entries.sort_by {|key, value| key}
		@entries.each do |key, value|
			keys[i] = key
			i+=1
		end
		keys
  end
  def printable
    string = []
		@entries.sort.each {|key,value| string << "\[#{key}\] \"#{value}\"" }
		string.join("\n")
  end
end
