class Timer
  attr_accessor :time_string
  attr_accessor :seconds
  def initialize
    @seconds = 0
  end
  def padded(value)
    if value == 0
      return '00'
    elsif value < 10 && value > 0
      return '0' + value.to_s
    else
      return value.to_s
    end
  end
  def seconds=(sec)
    if sec == 0
      @time_string = '00:00:' + padded(sec)
    elsif sec < 60 && sec > 0
      @time_string = '00:00:' + padded(sec)
    elsif sec > 60 && sec < 3600
      minute = (sec/60)
      sec1 = (sec%60)
      @time_string = '00:' + padded(minute) + ':' + padded(sec1)
    elsif sec >= 3600
      minute = (sec/60)%60
      hour = (sec/60)/60
      sec1 = sec%60
      @time_string = padded(hour) + ':' + padded(minute) + ':' + padded(sec1)
    end

  end

end
