class Book
  # TODO: your code goes here!
  attr_accessor :title
  def title=(name)
    name_array = name.split(' ')
    excluded = ['and','in','the','of','the','a','an']
    array2 = name_array.map do |word|
      if excluded.include?(word) == false
        word.capitalize
      else
        word 
      end
    end
    array2[0] = array2[0].capitalize
    @title = array2.join(' ')
  end

end
